##CODE CHALLENGE - Felipe de Aguiar

## COMO RODAR O PROJETO

1- É necessário ter o node.js instalado na maquina, por favor rode o npm i no CMD.
2- Entrando no caminho da pasta pelo CMD, rode ng serve --open, para abrir o projeto no navegador ou caso contrário, você pode abrir no VS Code (ou qualquer editor que se sinta mais confortável) e clicar em run que ele irá rodar.
