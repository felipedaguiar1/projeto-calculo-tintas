import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(){}
  title = 'Calculo de litros de tintas';

  public isValidacao: boolean;
  public isVisibleTextoFinal: boolean;

  public alturaParede1: number = 0; 
  public larguraParede1: number = 0; 

  public alturaParede2: number = 0; 
  public larguraParede2: number = 0;

  public alturaParede3: number = 0; 
  public larguraParede3: number = 0;

  public alturaParede4: number = 0; 
  public larguraParede4: number = 0;

  public qtdePortas1: number = 0;
  public qtdeJanelas1: number = 0;

  public qtdePortas2: number = 0;
  public qtdeJanelas2: number = 0;

  public qtdePortas3: number = 0;
  public qtdeJanelas3: number = 0;

  public qtdePortas4: number = 0;
  public qtdeJanelas4: number = 0;

  public qtdeTintaParede1: number
  public qtdeTintaParede2: number
  public qtdeTintaParede3: number
  public qtdeTintaParede4: number

  public textoTintas: any[]

  public realizarCalculo(alturaParede: number, larguraParede: number, qtdePortas: number, qtdeJanelas: number){
    var areaPorta =  0.80 * 1.90;
    var areaJanela = 2.00 * 1.20;
    
    var areaParede = alturaParede * larguraParede
    if(areaParede < 1 || areaParede > 15){
      return 0;
    }
    var porcenPortas = areaPorta * qtdePortas;
    var porcenJanelas = areaJanela * qtdeJanelas;

    var areaPortaseJanelas = porcenPortas + porcenJanelas;

    if(areaPortaseJanelas > areaParede / 2){
      return 0;
    }

    if(qtdePortas > 0 && alturaParede < 2.20){
      return 0;
    }

    var sub =  areaParede - areaPortaseJanelas

    var litros = sub / 5

    return litros

  }

  public gerarTintas(){
    this.isVisibleTextoFinal = false
    this.isValidacao =  false
    var listaTintas = [18, 3.6, 2.5, 0.5];
    let msgFinal: any[] = []
    this.qtdeTintaParede1 = this.realizarCalculo(this.alturaParede1, this.larguraParede1, this.qtdePortas1, this.qtdeJanelas1)
    this.qtdeTintaParede2 = this.realizarCalculo(this.alturaParede2, this.larguraParede2, this.qtdePortas2, this.qtdeJanelas2)
    this.qtdeTintaParede3 = this.realizarCalculo(this.alturaParede3, this.larguraParede3, this.qtdePortas3, this.qtdeJanelas3)
    this.qtdeTintaParede4 = this.realizarCalculo(this.alturaParede4, this.larguraParede4, this.qtdePortas4, this.qtdeJanelas4)

    if(this.qtdeTintaParede1 == 0 || this.qtdeTintaParede2 == 0 || this.qtdeTintaParede3 == 0 || this.qtdeTintaParede4 == 0){
      this.isValidacao =  true;
      return;
    }

    var somTintas = this.qtdeTintaParede1 + this.qtdeTintaParede2 + this.qtdeTintaParede3 + this.qtdeTintaParede4;
    var tintasComprar: any[] = [0,0,0,0]
    var i = 0

    for (i = 0; i < 4; i ++) {
      if(somTintas / listaTintas[i] < 1){
        continue;
      }

      tintasComprar[i]++;
      somTintas = somTintas - listaTintas[i]

      if(somTintas > 0){
        i = 0
        continue;
      }
    }

    if(somTintas > 0){
      tintasComprar[3]++
    }
    for(let y = 0; y < 4 ; y++){
      let teste = `Latas de ${listaTintas[y]}L: ${tintasComprar[y]}`
      msgFinal.push(teste)
    }
    this.textoTintas = msgFinal
    this.isVisibleTextoFinal = true
  }

  

}
